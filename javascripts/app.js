APP = {

		obj : {
			angle : 18.5,
			steps : 60,
			width : screen.width,
			height : screen.height,
			stepSize : 3
		},

	    sketchProc : function(processing) {
	        var Ps = processing;
	        var width = APP.obj.width;				// monitor width and height
	        var height = APP.obj.height;
	        
	        Ps.setup = function(){
	            Ps.size(width, height);
			    Ps.rectMode(Ps.CENTER);
			    Ps.noLoop();            
	        };
	        
			function Bubble() {
			    
			    var loc = new Ps.PVector( Ps.random(width*2), Ps.random(height) );
			    var radius = Ps.random( 5, 10 );
    
			    this.render = function()
			    {
			        for (var i = 1; i < 3; i++) {
			            Ps.ellipse( loc.x, loc.y, i * radius * 2, i * radius * 2 ); 
			        }
			    }  
			};

	        Ps.draw = function() {
				var flowerSize = Ps.random(height/5, height/3);
	                	
			    Ps.background(Ps.color( Ps.random(0, 255), Ps.random(0, 255), Ps.random(0, 255)));
			    Ps.smooth();
			    var fillColor = Ps.color( Ps.random(0, 255), Ps.random(0, 255), Ps.random(0, 255));
			    Ps.stroke(Ps.color( Ps.random(0, 255), Ps.random(0, 255), Ps.random(0, 255)), 200);

			    Ps.translate( width/2, height/2 );
			    for ( var i = 0; i < APP.obj.steps; i++ ) {
				    Ps.fill(fillColor, Ps.random(0, 255));
			        Ps.rotate( Ps.radians( APP.obj.angle * i ) );
			        Ps.rect( 0, 0, flowerSize - (APP.obj.stepSize * i), flowerSize - (APP.obj.stepSize * i) );
			    }
			    for ( var i = 0; i < APP.obj.steps; i++ ) {
			    	Ps.stroke(Ps.color( Ps.random(0, 255), Ps.random(0, 255), Ps.random(0, 255)), Ps.random(50, 100));
				    Ps.fill(fillColor, Ps.random(0, 255));
				    var b = new Bubble();
					b.render();				    
				}
	        };
	    },

	    makeMenu : function() {

			var gui = new dat.GUI();

			gui.add(APP.obj, 'about').name('About');
			var pt1 = gui.addFolder('Parameters');
			pt1.add(APP.obj, 'steps', 1, 100).name('Steps');
			pt1.add(APP.obj, 'angle', 1, 90).name('Angle');
			pt1.add(APP.obj, 'stepSize', 1, 10).step(1).name('Step size');
			// TODO: width and height resizing doesn't work
			// TODO: maybe it's better to work without dropdowns
			// pt1.add(obj, 'width', [1280,1440,1680,1920]).name('Width');
			// pt1.add(obj, 'height', [800,900,1050,1200]).name('Height');
			var pt2 = gui.addFolder('Actions');
			pt2.add(APP.obj, 'redraw').name('Redraw!');
			pt2.add(APP.obj, 'dl').name('Download!');	    	

	    }
}


