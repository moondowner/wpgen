
    var canvas = document.getElementById("sketch");
    var processingInstance = new Processing(canvas, APP.sketchProc);
    
    if (typeof(processingInstance) !== 'undefined') {

    	// TODO: move these to app.js;
	    APP.obj.redraw = function() {	
	    	processingInstance.redraw();	    	
	    }

	    APP.obj.dl = function() {
			window.location = canvas.toDataURL("image/png");	    	
	    }

	    APP.obj.about = function() {
	    	if (document.getElementById("message").className != "") {
	    		document.getElementById("message").className = "";
	    	} else {
	    		document.getElementById("message").className = "displayNone";	    		
	    	}
	    }

    	APP.makeMenu();
	}
